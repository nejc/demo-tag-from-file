from setuptools import setup, find_packages

setup(
    name='sample-project',
    version='1.0.1',
    url='https://gitlab.com/sample-project/sample-project.git',
    author='Author Name',
    author_email='author@gmail.com',
    description='Description',
    packages=find_packages(),
)
